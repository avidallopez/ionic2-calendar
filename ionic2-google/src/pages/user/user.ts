import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';
import { NativeStorage } from '@ionic-native/native-storage';
import { HomePage } from '../home/home';
import { UserModel } from './user.model';
declare var gapi: any;

/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  user: UserModel = new UserModel();
  userId: string = "";
  accessToken: string = "";
  APIKEY:string = "AIzaSyC_PXAXpC1a70x1_AJJInjr9Uih3Bnu0PM";
  calendarEvent:any = {};


  constructor(
    public navCtrl: NavController,
    public nativeStorage: NativeStorage,
    public googlePlus: GooglePlus) {}

  ionViewCanEnter(){
    this.nativeStorage.getItem('user')
      .then((data) => {
        this.user = {
          name: data.name,
          email: data.email,
          picture: data.picture
        };
        this.userId = data.userId;
        this.accessToken = data.accessToken;
      }, (error) => {
        console.log(error);
      });
  }

  doGoogleLogout(){
    let nav = this.navCtrl;
    this.googlePlus.logout()
      .then((response) => {
        this.nativeStorage.remove('user');
        nav.push(HomePage);
      },(error) => {
        console.log(error);
      })
  }

  createEvent(){
      var startDateTimeISO = this.buildISODate(this.calendarEvent.startDate, this.calendarEvent.startTime);
      var enddateTimeISO = this.buildISODate(this.calendarEvent.endDate, this.calendarEvent.endTime);

    //SENDING THE INVITE USING THE GOOGLE CALENDAR API
    gapi.client.setApiKey(this.APIKEY);
    var request = gapi.client.request({
      'path': '/calendar/v3/calendars/primary/events?alt=json',
      'method': 'POST',
      'headers': {
        'Authorization': 'Bearer ' + this.accessToken
      },
      'body': JSON.stringify({
        "summary": this.calendarEvent.name,
        "location": this.calendarEvent.location,
        "description": this.calendarEvent.description,
        "start": {
          "dateTime": startDateTimeISO,
          "timeZone": "Asia/Kolkata" // TODO : Parameterize this
        },
        "end": {
          "dateTime": enddateTimeISO,
          "timeZone": "Asia/Kolkata" // TODO : Parameterize this
        },
        "recurrence": [
          "RRULE:FREQ=DAILY;COUNT=1" //// TODO : Parameterize this, Frequency of the event
        ],
        "attendees": "",
        "reminders": {
          "useDefault": false,
          "overrides": [
            {
              "method": "email",
              "minutes": 1440   		// TODO : Parameterize this, No. of minutes before you want google services to send an email reminder
            },
            {
              "method": "popup",
              "minutes": 10 				// TODO : Parameterize this, No. of minutes before you want google services to send an popup reminder
            }
          ]
        }
      }),
      'callback': function (jsonR, rawR) {
        if(jsonR.id){
          console.log("Invitation sent successfully");
        } else {
          console.log("Failed to sent invite.");
        }
        console.log(jsonR); // Everything related to invite once created, use this for further enhancements
      }
    });
  }

  buildISODate(date, time){
    var dateArray = date && date.split('-');
    var timeArray = time && time.split(':');
    var normalDate = new Date(parseInt(dateArray[0]), parseInt(dateArray[1])-1, parseInt(dateArray[2]), parseInt(timeArray[0]), parseInt(timeArray[1]), 0, 0);
    return normalDate.toISOString();
  }



}
