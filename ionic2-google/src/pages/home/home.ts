import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { UserPage } from '../user/user';
import { GooglePlus } from '@ionic-native/google-plus';
import { NativeStorage } from '@ionic-native/native-storage';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public nativeStorage: NativeStorage,
    public googlePlus: GooglePlus) {}

  doGoogleLogin(){
    let nav = this.navCtrl;
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.googlePlus.login({
      'scopes':'https://www.googleapis.com/auth/calendar',
      'offline': true
    })
      .then(user => {
        console.log("---->",user);
        loading.dismiss();
        this.nativeStorage.setItem('user', {
          name: user.displayName,
          email: user.email,
          picture: user.imageUrl,
          accessToken: user.accessToken,
          userId: user.userId
        }).then(() => {
          nav.push(UserPage);
        }, (error) => {
          console.log(error);
        })
      })
      .catch(err => {
        console.error("---->",err);
        loading.dismiss();
      });
  }
}
